//
//  ContactsViewController.swift
//  Contacts
//
//  Created by Инна Шамрай on 22.06.2021.
//
import Foundation
import UIKit

class ContactsViewController: UIViewController {
    
    @IBOutlet weak var contactsTableView: UITableView!
    
    var arrOfContacts = [ContactModel]()
    var arrOfMobilePhones = [MobileModel]()
    var arrOfHomePhones = [MobileModel]()
    
    func mockPhones () {
        arrOfMobilePhones.append(MobileModel(mobileText: "Mobile Phone", mobileNumber: "09988767583"))
        arrOfMobilePhones.append(MobileModel(mobileText: "Mobile Phone", mobileNumber: "09567900754"))
        arrOfMobilePhones.append(MobileModel(mobileText: "Mobile Phone", mobileNumber: "09988778904"))
        arrOfMobilePhones.append(MobileModel(mobileText: "Mobile Phone", mobileNumber: "096789000987"))
        
        arrOfHomePhones.append(MobileModel(mobileText: "", mobileNumber: ""))
        arrOfHomePhones.append(MobileModel(mobileText: "", mobileNumber: "0967897865"))
        arrOfHomePhones.append(MobileModel(mobileText: "", mobileNumber: "0675674521"))
        arrOfHomePhones.append(MobileModel(mobileText: "", mobileNumber: ""))
    }
    
    func mockData (){
        
        arrOfContacts.append(ContactModel(name: "Bob", image: UIImage(named: "Bob")))
        arrOfContacts.append(ContactModel(name: "Jack", image: UIImage(named: "Jack")))
        arrOfContacts.append(ContactModel(name: "Anna", image: UIImage(named: "Anna")))
        arrOfContacts.append(ContactModel(name: "Bill", image: UIImage(named: "Bill")))
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactsTableView.register(UINib(nibName: "NamePhotoTableViewCell", bundle: nil), forCellReuseIdentifier: "NamePhotoTableViewCellIdn")
        mockPhones()
        mockData()
        contactsTableView.dataSource = self
        contactsTableView.delegate = self
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ContactsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NamePhotoTableViewCellIdn") as? NamePhotoTableViewCell {
            
            let item = arrOfContacts[indexPath.row]
            
            cell.nameLabel.text = item.name
            cell.namePhoto.image = item.image
            
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfContacts.count
    }
    
}

extension ContactsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard?.instantiateViewController(identifier: "ContactsDetailsViewController") as? ContactsDetailsViewController else {return}
        
        vc.contact = arrOfContacts[indexPath.row]
        vc.mobileMobile = arrOfMobilePhones[indexPath.row]
        vc.mobileHome = arrOfHomePhones[indexPath.row]
       // self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

