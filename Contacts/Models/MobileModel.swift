//
//  MobileModel.swift
//  Contacts
//
//  Created by Инна Шамрай on 25.06.2021.
//

import UIKit

struct MobileModel {
    
    var mobileText: String
    var mobileNumber: String?
    
}
