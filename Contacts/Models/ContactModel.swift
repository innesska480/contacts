//
//  ContactModel.swift
//  Contacts
//
//  Created by Инна Шамрай on 23.06.2021.
//

import UIKit

struct ContactModel {
    
    let name: String
    let image: UIImage?
        
}
