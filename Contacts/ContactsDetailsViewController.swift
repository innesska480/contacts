//
//  ContactsDetailsViewController.swift
//  Contacts
//
//  Created by Инна Шамрай on 24.06.2021.
//

import UIKit

class ContactsDetailsViewController: UIViewController {
    
    enum cells {
        case namePhoto
        case connectors
        case mobilePhone
        case homePhone
    }
    var arrOfMobilePhones : [cells?] = [.mobilePhone]
    var arrOfHomePhone : [cells?] = [.homePhone]
    
    var number = "09510000345"
    var number1 = "0951056785"
    
    private let item : [cells] = [.namePhoto, .connectors, .mobilePhone, .homePhone]
    @IBOutlet weak var detailsTableView: UITableView!
    
    var contact : ContactModel?
    var mobileMobile : MobileModel?
    var mobileHome : MobileModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsTableView.register(UINib(nibName: "MobilePhoneTableViewCell", bundle: nil), forCellReuseIdentifier: "MobilePhoneTableViewCellIdn")
        
        detailsTableView.register(UINib(nibName: "ConectorsTableViewCell", bundle: nil), forCellReuseIdentifier: "ConectorsTableViewCellIdn")
        
        detailsTableView.register(UINib(nibName: "NamePhotoDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "NamePhotoDetailsTableViewCellIdn")
        
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        
    
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ContactsDetailsViewController : UITableViewDelegate {
    
}
extension ContactsDetailsViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = item[indexPath.row]
        switch item {
        case .namePhoto :
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NamePhotoDetailsTableViewCellIdn") as? NamePhotoDetailsTableViewCell {
                
                //     let item = arrOfContacts[indexPath.row]
                cell.contactName.text = contact?.name
                cell.contactPhoto.image = contact?.image
                
                return cell
            }
        case .connectors :
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConectorsTableViewCellIdn") as? ConectorsTableViewCell
            
            //
            
            return cell!
        case .mobilePhone :
            let cell = tableView.dequeueReusableCell(withIdentifier: "MobilePhoneTableViewCellIdn") as? MobilePhoneTableViewCell
            mobileMobile?.mobileText = "Mobile Phone"
            cell?.mobileNumberLabel.textColor = UIColor.systemBlue
            cell?.mobileTextLabel.text = mobileMobile?.mobileText
            cell?.mobileNumberLabel.text = mobileMobile?.mobileNumber
//            cell?.backgroundColor = UIColor.black
            return cell!
            
        case .homePhone :
            let cell = tableView.dequeueReusableCell(withIdentifier: "MobilePhoneTableViewCellIdn") as? MobilePhoneTableViewCell
            mobileHome?.mobileText = "Home Phone"
            cell?.mobileNumberLabel.textColor = UIColor.systemBlue
            cell?.mobileTextLabel.text = mobileHome?.mobileText
            cell?.mobileNumberLabel.text = mobileHome?.mobileNumber
            //cell?.backgroundColor = UIColor.green
            return cell!
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
        
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return item.count
    }
    
}
