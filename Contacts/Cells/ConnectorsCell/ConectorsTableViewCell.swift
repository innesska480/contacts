//
//  ConectorsTableViewCell.swift
//  Contacts
//
//  Created by Инна Шамрай on 24.06.2021.
//

import UIKit

class ConectorsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageButton.layer.cornerRadius = 20
        messageButton.clipsToBounds = true
        
        callButton.layer.cornerRadius = 20
        callButton.clipsToBounds = true
        
        videoButton.layer.cornerRadius = 20
        videoButton.clipsToBounds = true
        
        mailButton.layer.cornerRadius = 20
        mailButton.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
