//
//  MobilePhoneTableViewCell.swift
//  Contacts
//
//  Created by Инна Шамрай on 24.06.2021.
//

import UIKit

class MobilePhoneTableViewCell: UITableViewCell {

    @IBOutlet weak var mobileTextLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
