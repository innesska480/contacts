//
//  NamePhotoDetailsTableViewCell.swift
//  Contacts
//
//  Created by Инна Шамрай on 25.06.2021.
//

import UIKit

class NamePhotoDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var contactPhoto: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
